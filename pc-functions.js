
let lastMessage = '';
function log(message) {
    if (message == lastMessage) {
        return;
    } else {
        lastMessage = message;
    }
    let timeStamp = new Date();
    console.log(timeStamp.toLocaleString() + " pc: " + message);
}

function standBy() {
    log('standby');
}

function onTime(delta) {
    log('on time ' + delta.getMinutes() + ' minutes');
}

function late(delta) {
    log('late ' + delta.getMinutes() + ' minutes');
}

function reallyLate(delta) {
    log('really late ' + delta.getMinutes() + ' minutes');
}

function error() {
    log('error');
}

function noInfo(message) {
    log(message);
}

function endProgram() {

}

module.exports = {
    standBy,
    onTime,
    late,
    reallyLate,
    noInfo,
    error,
    endProgram,
};
