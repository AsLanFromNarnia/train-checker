
var Gpio = require('onoff').Gpio;   // Include onoff to vareract with the GPIO.

// Use GPIO pins and specify they are output.

var ledRows = new Array();
var ledCols = new Array();

ledRows.push(new Gpio(10, 'out'));
ledRows.push(new Gpio(22, 'out'));
ledRows.push(new Gpio(27, 'out')); // 21 for pi generation 1, otherwise 27.
ledRows.push(new Gpio(17, 'out'));

ledCols.push(new Gpio(8, 'out'));
ledCols.push(new Gpio(25, 'out'));
ledCols.push(new Gpio(24, 'out'));
ledCols.push(new Gpio(23, 'out'));

// LED state buffer.

var leds = new Array(ledRows.length);

for (var i = 0; i < leds.length; i++)
{
    leds[i] = new Array(ledCols.length);
}

function refresh()
{
    // Cols are connected to LED anode (+).

    for (let i = 0; i < ledRows.length; i++) {
        ledRows[i].writeSync(1);
    }

    for (let i = 0; i < ledCols.length; i++) {
        ledCols[i].writeSync(0);
    }

    for (let i = 0; i < leds.length; i++) {
        let rowIsLit = false;
        for (let j = 0; j < leds[i].length; j++) {
            if (leds[i][j] == 1) {
                ledCols[j].writeSync(1);
                rowIsLit = true;
            }
        }

        if (rowIsLit) {
            ledRows[i].writeSync(0);
        }
    }
}

function clearLEDS()
{
    for (var i = 0; i < leds.length; i++)
    {
        for (var j = 0; j < leds[i].length; j++)
        {
            leds[i][j] = 0;
        }
    }

    refresh();
}

function unexportOnClose()
{
    clearLEDS();
    
    for (var i = 0; i < ledRows.length; i++)
    {
        ledRows[i].unexport();
    }

    for (var i = 0; i < ledCols.length; i++)
    {
        ledCols[i].unexport();
    }
}

process.on('SIGINT', unexportOnClose); // Function to run when user closes using ctrl+c.

clearLEDS();

// Interface for train-checker

let lastMessage = '';
function log(message) {
    if (message == lastMessage) {
        return;
    } else {
        lastMessage = message;
    }
    let timeStamp = new Date();
    console.log(timeStamp.toLocaleString() + " led: " + message);
}

function standBy() {
    log('standby');
    clearLEDS();
    leds[0][3] = 1;
    refresh();
}

function onTime(delta) {
    log('on time ' + delta.getMinutes() + ' minutes');
    clearLEDS();
    leds[0][2] = 1;
    refresh();
}

function late(delta) {
    log('late ' + delta.getMinutes() + ' minutes');
    clearLEDS();
    leds[0][1] = 1;
    refresh();
}

function reallyLate(delta) {
    log('really late ' + delta.getMinutes() + ' minutes');
    clearLEDS();
    leds[0][0] = 1;
    refresh();
}

function noInfo(message) {
    log(message);
    clearLEDS();
    leds[0][3] = 1;
    leds[0][2] = 1;
    refresh();
}

function error() {
    log('error');
    clearLEDS();
    leds[0][0] = 1;
    leds[0][3] = 1;
    leds[3][0] = 1;
    leds[3][3] = 1;
    refresh();
}

function endProgram() {
    unexportOnClose();
}

module.exports = {
    standBy,
    onTime,
    late,
    reallyLate,
    noInfo,
    error,
    endProgram,
};
