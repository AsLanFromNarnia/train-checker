const fetch = require('node-fetch');

let lastMessage = '';
function log(message) {
    if (message == lastMessage) {
        return;
    } else {
        lastMessage = message;
    }
    let timeStamp = new Date();
    if (typeof message === 'string') {
        console.log(timeStamp.toLocaleString() + " checker: " + message);
    } else {
        // Don't concatenate non strings.
        console.log(timeStamp.toLocaleString() + " checker: ");
        console.log(message);
    }
    
}

function fetchJson(url) {
    let promise = new Promise((resolve, reject) => {
        fetch(url).then(response => {
            response.json().then(json => {
                resolve(json);
            }, err => {
                log(err);
                reject(err);
            });
        }, err => {
            log(err);
            reject(err);
        });
    });

    return promise;
}

const baseUrl = 'https://api-v3.mbta.com/';
const route = 'CR-Fitchburg';

function updateUrls(trainNumber) {        
    // This gets the trip names for the route e.g.
    // https://api-v3.mbta.com/trips?filter[route]=CR-Fitchburg

    let tripsUrl = baseUrl + 'trips?filter[route]=' + route;

    let urlsPromise = new Promise((resolve, reject) => {
        fetchJson(tripsUrl).then(response => {

            let urls = [];

            for (let i = 0; i < response.data.length; i++) {
                if (response.data[i].attributes.name == trainNumber) {
                    let trip = response.data[i].id;
                    let scheduleUrl = baseUrl + 'schedules?filter[trip]=' + trip;
                    let predictionUrl = baseUrl + 'predictions?filter[trip]=' + trip;
                    
                    urls.push({
                        scheduleUrl: scheduleUrl,
                        predictionUrl: predictionUrl,
                    });
                }
            }

            if (urls.length > 0) {
                log(urls);
                resolve(urls);
            } else {
                log('Unable to retrieve trip from trips query!');
                reject('Unable to retrieve trip from trips query!');
            }

        }, err => {
            log(err);
            reject(err);
        });
    });

    return urlsPromise;
}

function getSchedule(scheduleUrl) {
    return fetchJson(scheduleUrl);
}

function getPredictions(predictionUrl) {
    return fetchJson(predictionUrl);
}

module.exports = {
    updateUrls,
    getSchedule,
    getPredictions,
};
