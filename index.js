
let hostname = require('os').hostname;
let checker = require('./checker');
let display;

let lastMessage = '';
function log(message) {
    if (message == lastMessage) {
        return;
    } else {
        lastMessage = message;
    }
    let timeStamp = new Date();
    console.log(timeStamp.toLocaleString() + " index: " + message);
}

if (hostname() === 'pizero') {
    log('using led-functions');
    display = require('./led-functions');
} else {
    log('using pc-functions');
    display = require('./pc-functions');
}

function getLincolnScheduledTime(schedule) {
    if (!schedule.data) {
        log('failed to retrieve schedule');
        display.error();
        return null;
    }

    // Get the time expected at Lincoln.
    let scheduledTime = null;
    for (let i = 0; i < schedule.data.length; i++) {
        if (schedule.data[i].relationships.stop.data.id == 'Lincoln') {
            scheduledTime = new Date(schedule.data[i].attributes.arrival_time);
            break;
        }
    }

    return scheduledTime;
}

function checkTrain(leavingTime, scheduledTime, predictionUrl) {
    // Get the current time.
    let currentTime = new Date();        
    if (currentTime < leavingTime) {
        display.standBy();
        return;
    }

    // Get predictions.
    checker.getPredictions(predictionUrl).then(predictions => {
        // Find Lincoln.
        for (let i = 0; i < predictions.data.length; i++) {
            if (predictions.data[i].relationships.stop.data.id == 'Lincoln') {

                let predictedTime = new Date(predictions.data[i].attributes.arrival_time);

                if (predictedTime.valueOf() == 0) {
                    display.noInfo('prediction is 0');
                    return;
                }

                if (predictedTime < scheduledTime) {
                    // Train is early
                    display.onTime(new Date(0));
                    return;
                }

                let delta = new Date(predictedTime - scheduledTime);
                
                // If the delta is greater than 5 minutes, show the train as really late.
                // If the delta is greater than 2 minutes, show the train as late.

                if (delta > 300000) {
                    display.reallyLate(delta);

                } else if (delta > 120000) {
                    display.late(delta);

                } else {
                    display.onTime(delta);
                }

                return;
            }
        }

        // If Lincoln is not found, show an error.
        display.noInfo('cannot find Lincoln prediction');
    });
}

function checkSchedule(urls, index) {
    if (index >= urls.length) {
        tryAgainInFiveMinutes('Failed to find Lincoln schedule in URLs');
        return;
    }

    let scheduleUrl = urls[index].scheduleUrl;
    log('Checking ' + scheduleUrl);

    // Get the schedule, then check every minute until train leaves Lincoln.
    checker.getSchedule(scheduleUrl).then((response) => {
        let schedule = response;
        let lincolnScheduledTime = getLincolnScheduledTime(schedule);

        if (lincolnScheduledTime == null) {
            checkSchedule(urls, index + 1);
            return;
        }

        // Get the station leaving time too.
        let leavingTime = new Date(schedule.data[0].attributes.departure_time);

        // Check the train now and set up an interval
        checkTrain(leavingTime, lincolnScheduledTime, urls[index].predictionUrl);
        setInterval(checkTrain, 60000, leavingTime, lincolnScheduledTime, urls[index].predictionUrl);
    }, () => {
        log('getSchedule failed!');
        checkSchedule(urls, index + 1);
        return;
    });
}

function endProgram() {
    log('ending program');
    display.endProgram();
    process.exit(0);
}

function tryAgainInFiveMinutes(message) {
    log(message);
    display.error();
    // Try again in 5 minutes.
    setTimeout(main, 300000);
}

// Start
log('starting train-checker');

function main() {

    let trainNumber = '406';
    if (process.argv.length == 3) {
        trainNumber = process.argv[2];
    }

    // Update the URLs.
    checker.updateUrls(trainNumber).then((urls) => {
        // urls is an array of potential schedules.
        if (urls.length < 1) {
            tryAgainInFiveMinutes('updateUrls failed!');
            return;
        }

        // Check schedule, will call itself if necessary
        // to iterate through urls, and call checkTrain
        // when correct schedule is found.
        checkSchedule(urls, 0);
    }, () => {
        tryAgainInFiveMinutes('updateUrls failed!');
        return;
    });
}

// End the program after 80.5 minutes.
let quitInterval = setTimeout(endProgram, 4830000);

main();
