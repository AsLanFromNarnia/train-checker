
let display = require('./led-functions');

function parseInput(d) {

	// note:  d is an object, and when converted to a string it will
    // end with a linefeed.  so we (rather crudely) account for that  
    // with toString() and then trim() 
    // console.log("you entered: [" + d.toString().trim() + "]");
    
    let delta = new Date();

	var c = d.toString().trim();
    
	if (c == '1')
	{	
		display.standBy();
	}
	else if (c == '2')
	{
		display.onTime(delta);
	}
	else if (c == '3')
	{		
		display.late(delta);
    }
    else if (c == '4')
    {
        display.reallyLate(delta);
    }
    else if (c == '5')
    {
        display.noInfo('test noInfo');
    }
    else if (c == '6')
    {
        display.error();
    }
    else if (c == 'q')
    {
        display.endProgram();
        console.log('Goodbye!');
        process.exit(0);
    }
	else
	{
		console.log('Unknown option.');
		console.log('Enter 1-6 or \'q\' for quit.');
	}
}

var stdin = process.openStdin();

stdin.addListener('data', parseInput);

console.log('Enter 1-6 or \'q\' for quit.');
